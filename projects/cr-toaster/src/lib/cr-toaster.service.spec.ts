import { TestBed } from '@angular/core/testing';

import { CrToasterService } from './cr-toaster.service';

describe('CrToasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrToasterService = TestBed.get(CrToasterService);
    expect(service).toBeTruthy();
  });
});
