import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class CrToasterService {
  defaultOptions = {
    panelClass: '',
    horizontalPosition: 'center',
    verticalPosition: 'bottom',
    duration: 3000
  };

  constructor(private snackBar: MatSnackBar) { }

  success(message, options = {}) {
    this.defaultOptions.panelClass = 'success-dialog';
    const config: any = { ...this.defaultOptions, ...options };
    this.snackBar.open(message || 'Success', null, config);
  }

  warning(message, options = {}) {
    this.defaultOptions.panelClass = 'warning-dialog';
    const config: any = { ...this.defaultOptions, ...options };
    this.snackBar.open(message || 'Something went wrong', null, config);
  }

  error(message, options = {}) {
    this.defaultOptions.panelClass = 'error-dialog';
    const config: any = { ...this.defaultOptions, ...options };
    this.snackBar.open(message || 'Something went wrong', null, config);
  }
}
