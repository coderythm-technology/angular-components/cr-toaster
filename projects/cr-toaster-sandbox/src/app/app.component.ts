import { Component } from '@angular/core';
import { CrToasterService } from 'projects/cr-toaster/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cr-toaster-sandbox';

  constructor(private crToasterService: CrToasterService) {

  }

  showToaster() {
    this.crToasterService.success('It Works');
  }
}
